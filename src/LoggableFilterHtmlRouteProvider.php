<?php

namespace Drupal\loggable;

use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;

/**
 * Provides routes for Loggable filter entities.
 *
 * @see Drupal\Core\Entity\Routing\AdminHtmlRouteProvider
 * @see Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider
 */
class LoggableFilterHtmlRouteProvider extends AdminHtmlRouteProvider {

}
