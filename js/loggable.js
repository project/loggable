(function (Drupal) {
  'use strict';

  Drupal.Loggable = {};

  var uid = '';
  if (drupalSettings.uid != '') {
    uid = drupalSettings.uid;
  }

  var url = '';
  if (window.location.href != '') {
    url = window.location.href;
  }

  var channel_id = '';
  if (drupalSettings.channel_id != '') {
    channel_id = drupalSettings.channel_id;
  }
  else {
    return false;
  }

  var api_key = '';
  if (drupalSettings.api_key != '') {
    api_key = drupalSettings.api_key;
  }
  else {
    return false;
  }

  var domain = '';
  if (drupalSettings.domain != '') {
    domain = drupalSettings.domain;
  }
  else {
    return false;
  }

  /*
  *
  * Redefine "console" and "window.console"
  * 
  * Calling postFetch method for sending info to server
  * 
  */
  var item = '';
  var console = (function (oldCons) {
    return {
      log: function (msg) {
        oldCons.log(msg);
        item = {
          type: 'console',
          severity: 'info',
          user: uid,
          url: url,
          message: 'console.log: ' + msg
        }
        Drupal.Loggable.postFetch(item);
      },
      info: function (msg) {
        oldCons.info(msg);
        item = {
          type: 'console',
          severity: 'info',
          user: uid,
          url: url,
          message: 'console.info: ' + msg
        }
        Drupal.Loggable.postFetch(item);
      },
      warn: function (msg) {
        oldCons.log(msg);
        item = {
          type: 'console',
          severity: 'info',
          user: uid,
          url: url,
          message: 'console.warn: ' + msg
        }
        Drupal.Loggable.postFetch(item);
      },
      error: function (msg) {
        oldCons.error(msg);
        item = {
          type: 'console',
          severity: 'error',
          user: uid,
          url: url,
          message: 'console.error: ' + msg
        }
        Drupal.Loggable.postFetch(item);
      },
      debug: function (msg) {
        oldCons.debug(msg);
        item = {
          type: 'console',
          severity: 'debug',
          user: uid,
          url: url,
          message: 'console.debug: ' + msg
        }
        Drupal.Loggable.postFetch(item);
      }
    };
  }(window.console));
  window.console = console;

  /*
  *
  * If get the error via window.addEventListener
  *
  * Capture this error to "$item"
  * 
  * Console.log this error
  * 
  */
  window.addEventListener('error', function (event) {
    console.log(event)
    item = ''
    item = {
      type: 'console',
      severity: 'error',
      user: uid,
      url: url,
      message: 'window.addEventListener.error: ' + event.message + '. file: ' + event.filename + '. Line: ' + event.lineno
    }
    Drupal.Loggable.postFetch(item);
    return false;
  });

  /*
  *
  * If get the error via window.addEventListener
  *
  * Capture this error to "$item"
  * 
  * Console.log this error
  *
  */
  window.addEventListener('unhandledrejection', function (event) {
    item = ''
    item = {
      type: 'console',
      severity: 'error',
      user: uid,
      url: url,
      message: 'window.addEventListener: ' + event.message + '. file: ' + event.filename + '. Line: ' + event.lineno
    }
    console.log(event)
    Drupal.Loggable.postFetch(item);
    return false;
  });

  /*
  *
  * Send info to server loggable.zanzarra.com
  *
  * Native JS, function "fetch"
  *
  */
  Drupal.Loggable.postFetch = function (data) {
    if (domain != '' && api_key != '' && channel_id != '') {
      fetch(domain + "/api/event/?api-key=" + api_key, {
        method: "POST",
        headers: {
          "Accept": "application/vnd.api+json",
          "Content-Type": "application/vnd.api+json",
          "api-key": api_key
        },
        body: JSON.stringify({
          "data": {
            "type": "event",
            "attributes": {
              "type": data['type'],
              "severity": data['severity'],
              "user": data['user'],
              "url": data['url'],
              "message": data['message'],
            },
            "relationships": {
              "channel": {
                "data": {
                  "type": "channel",
                  "id": channel_id
                }
              }
            }
          }
        })
      })
    }
    else{
      return false;
    }
  }
})(Drupal);
